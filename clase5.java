import java.util.Scanner;
public class Main {
    public static void main(String[] args) {     
           Scanner sc = new Scanner(System.in);  //crear un objeto Scanner
           int n;
           
           System.out.print("Introduzca un número entero: ");
           n = sc.nextInt(); //leer un entero
           System.out.println("El cuadrado es: " + Math.pow(n,2));
     }
}